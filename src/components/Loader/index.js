import {
    GLTFLoader
  } from 'three/examples/jsm/loaders/GLTFLoader';
  import {
    OBJLoader
  } from 'three/examples/jsm/loaders/OBJLoader';
  import {
    FBXLoader
  } from 'three/examples/jsm/loaders/FBXLoader';
  import { TextureLoader } from 'three';
  
  
  export default class Loader {
    constructor(viewer) {
  
      this.viewer = viewer;
      this.GLTFLoader = new GLTFLoader();
      this.OBJLoader = new OBJLoader();
      this.FBXLoader = new FBXLoader();
      this.TextureLoader = new TextureLoader();
    }
  
    loadModel(path, callback, format) {
      
      let modelType = format ? format.split('.').pop().split('?')[0] : path.split('.').pop().split('?')[0];
      switch (modelType) {
        case 'gltf': {
          this.GLTFLoader.load(path, (gltf) => {
            this.addModel(gltf, callback);
          });
          break;
        }
        case 'obj': {
          this.OBJLoader.load(path, (obj) => {
            this.addModel(obj, callback);
          });
          break;
        }
        case 'fbx': {
          this.FBXLoader.load(path, (obj) => {
            this.addModel(obj, callback);
          });
          break;
        }
        case 'glb': {
          this.GLTFLoader.load(path, (glb) => {
            this.addModel(glb, callback);
          });
          break;
        }
        default: 
      }
    }
  
    loadTexture(path, callback) {
      this.TextureLoader.load(path, callback)
    }
  
    addModel(obj, callback) {
      obj.scene ? this.viewer.scene.add(obj.scene) : this.viewer.scene.add(obj);
      if (callback) callback(obj);
    }
  }
  