import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader";
import hdr from "./../../assets/envmap.hdr";
import px from "./../../assets/envmap/px.png";
import py from "./../../assets/envmap/py.png";
import nx from "./../../assets/envmap/nx.png";
import ny from "./../../assets/envmap/ny.png";
import pz from "./../../assets/envmap/pz.png";
import nz from "./../../assets/envmap/nz.png";
import denim from "./../../assets/backpack/denim_baseColor.jpg";
import denimNormal from "./../../assets/backpack/denim_baseColor.jpg";
import denimOcclusion from "./../../assets/backpack/denim_baseColor.jpg";
import * as dat from "dat.gui";
// import Loader from '../Loader';
import fakeShadowTexture from "./../../assets/shadow.png";
import { VRButton } from "../VRButton";

export default class Viewer {
  constructor() {
    // this.gui = new dat.GUI();
    this.DOMCanvasElement = document.querySelector("canvas.webgl");
    this.objectState = {
      material: "Denim",
      color: "Brown",
      furniture: "Silver",
    };
    this.init();
  }

  init() {
    this.initScene();
    this.initCamera();
    window.addEventListener("resize", this.onWindowResize.bind(this));
    this.initControls();
    this.initLights();
    this.addFloor();
    this.initEnvMap();
    this.addVRButton(this.renderer);
    this.animate();
  }

  initLights() {
    let ambient = new THREE.AmbientLight(0xffffff, 0.9);
    ambient.name = "AmbientLight";
    ambient.position.set(10, 200, 2);

    let directional = new THREE.DirectionalLight(0xffffff, 0.5);
    directional.name = "DirectionalLight";
    directional.position.set(15, 12, 1);

    this.scene.add(ambient);
    this.scene.add(directional);
  }

  initScene() {
    // Canvas
    this.container = this.DOMCanvasElement;

    // Scene
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.CubeTextureLoader().load([
      px,
      nx,
      py,
      ny,
      pz,
      nz,
    ]);

    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      canvas: this.container,
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
  }

  initCamera() {
    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      100
    );
    this.camera.position.x = 0;
    this.camera.position.y = 0.2;
    this.camera.position.z = 1;
    this.scene.add(this.camera);
  }

  initEnvMap() {
    const pmremGenerator = new THREE.PMREMGenerator(this.renderer);
    pmremGenerator.compileEquirectangularShader();
    new RGBELoader().load(hdr, (texture) => {
      let envMap = pmremGenerator.fromEquirectangular(texture).texture;
      this.scene.environment = envMap;
      texture.dispose();
      pmremGenerator.dispose();
    });
  }

  addFloor() {
    const radius = 10;
    const segments = 32;
    const geometry = new THREE.CircleGeometry(radius, segments);
    const material = new THREE.MeshStandardMaterial({
      color: 0xffffff,
      roughness: 0.85,
      metalness: 0.6,
    });
    const floor = new THREE.Mesh(geometry, material);
    floor.receiveShadow = true;
    floor.rotation.x = -Math.PI / 2;
    this.scene.add(floor);
  }

  createFakeShadow() {
    let shadowGeometry = new THREE.PlaneGeometry(0.6, 0.6);
    let shadowTexture = new THREE.TextureLoader().load(fakeShadowTexture);
    let shadowMaterial = new THREE.MeshStandardMaterial({
      map: shadowTexture,
      transparent: true,
      opacity: 0.2,
      side: THREE.DoubleSide,
    });
    let shadow = new THREE.Mesh(shadowGeometry, shadowMaterial);
    shadow.rotation.x = Math.PI / 2;
    shadow.position.set(-0.1, 0.001, -0.03);
    this.scene.add(shadow);
  }

  onWindowResize() {
    this.camera.aspect =
      this.container.offsetWidth / this.container.offsetHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(
      window.innerWidth,
      window.innerHeight
    );
  }

  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.enablePan = false;
    // this.controls.minPolarAngle = Math.PI / 1.2;
    this.controls.maxPolarAngle = Math.PI / 1.85;
    this.controls.target.set(0, 0.2, 0);
    // this.controlsParams = {
    //   distance: {
    //     curr: this.camera.position.distanceTo( this.controls.target ),
    //     min: 3,
    //     max: 7
    //   },
    // }
    const cameraDistance = window.innerWidth > 768 ? 4 : 6;
    this.controls.minDistance = 0.4;
    this.controls.maxDistance = 1;
    this.currentDistance = cameraDistance;
    // this.controls.enabled = false;
  }

  addVRButton(renderer) {
    document.body.appendChild(VRButton.createButton(renderer));
    renderer.xr.enabled = true;
  }

  changeZoom(action) {
    let currDistance = this.camera.position.distanceTo(this.controls.target);

    if (action === "plus" && currDistance !== this.controls.minDistance) {
      new TWEEN.Tween(this.camera.position)
        .to(
          {
            x: this.camera.position.x / 1.3,
            y: this.camera.position.y / 1.3,
            z: this.camera.position.z / 1.3,
          },
          300
        )
        .start();
    }

    if (action === "minus" && currDistance !== this.controls.maxDistance) {
      new TWEEN.Tween(this.camera.position)
        .to(
          {
            x: this.camera.position.x * 1.3,
            y: this.camera.position.y * 1.3,
            z: this.camera.position.z * 1.3,
          },
          300
        )
        .start();
    }
  }

  setDefaultModelOptions(obj) {
    const denimMap = new THREE.TextureLoader().load(denim);
    const denimNormalMap = new THREE.TextureLoader().load(denimNormal);
    const denimOcclusionMap = new THREE.TextureLoader().load(denimOcclusion);
    const baseColor = new THREE.Color("#9f7356").getHex();
    const baseStrapColor = new THREE.Color("#9f7356").getHex();
    const baseFurnitureColor = new THREE.Color("#fefefe").getHex();

    obj.scene.traverse((el) => {
      if (el.name === "Mesh") {
        el.material.map = denimMap;
        el.material.normalMap = denimNormalMap;
        el.material.roughnessMap = denimOcclusionMap;
        el.material.metalnessMap = denimOcclusionMap;
      }
      if (el.name === "Mesh" || el.name === "Mesh_2") {
        el.material.color.set(el.name === "Mesh" ? baseColor : baseStrapColor);
      }
      if (el.name === "Mesh_1") {
        el.material.color.set(baseFurnitureColor);
      }
      
    });
  }

  rotateCamera() {
    this.controls.autoRotate = !this.controls.autoRotate;
  }

  animate() {
    
    this.renderer.setAnimationLoop(() => {
      this.render();
    })
    
  }

  render() {
    this.controls.update();
    this.renderer.render(this.scene, this.camera);
  }
}
