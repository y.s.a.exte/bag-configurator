import {
    SphereGeometry,
    MeshBasicMaterial,
    Mesh,
    PlaneGeometry,
    VideoTexture,
    DoubleSide,
    TextureLoader,
  } from 'three';
  import Viewer from './Viewer';
import Loader from './Loader';
//   import Navigation from '../navigation';
import model from './../assets/backpack/backpack.glb';
import Navigation from './Navigation';
import Loading from './Loading';
  
  export default class App {
    constructor() {
      this.loading = new Loading();
      this.loading.show();
      this.viewer = new Viewer();
      this.loader = new Loader(this.viewer);
      this.navigation = new Navigation(this.viewer);
    //   this.navigation = new Navigation(this.viewer);
      this.initModel();
      this.addQR();
    }

    initModel() {
        this.loader.loadModel(model, (obj) => {
            this.viewer.setDefaultModelOptions(obj);
            this.viewer.createFakeShadow();
            this.loading.hide();
            this.viewer.modelObject = obj;
        })
    }

    addQR() {
      let qr = document.createElement("div");
      qr.classList.add("qr");
      qr.innerHTML = `<img src='https://cdn.me-qr.com/qr/55219576.png?v=1680720781' alt='qr'>`
      document.body.appendChild(qr)

      qr.addEventListener('click', () => {
        qr.classList.toggle('focused');
      })
    }

  }
  