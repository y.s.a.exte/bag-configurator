import { Color, TextureLoader } from "three";
import { materials, colors, furnitures} from "../../data/navigation";

export default class Navigation {
  constructor(viewer) {
    this.navListItems = [{
      label: "Material",
      options: materials,
    }, 
    {
        label: "Color",
        options: colors,
    },
    {
        label:  "Furniture",
        options: furnitures,
    }];
    this.viewer = viewer;
    this.activeMenuItem = null;
    this.init();
  }

  init() {
    this.createNavigation();
    this.createSettingsBar();
  }

  createNavigation() {
    this.nav = document.createElement("nav");
    this.nav.classList.add("nav");
    this.navList = document.createElement("ul");
    this.navList.classList.add("nav-list");
    this.nav.appendChild(this.navList);
    this.navListItems.forEach((item, index) => {
      const navItem = document.createElement("li");
      navItem.textContent = item.label;
      navItem.classList.add("nav-list__item");
      navItem.dataset.itemId = index;
      this.navList.appendChild(navItem);
      this.handleNavItemClick(item);

      navItem.addEventListener('click', (ev) => {
        this.handleNavItemClick(ev?.target)
      })
    });

    document.body.appendChild(this.nav);
  }

  createSettingsBar() {
    this.settingsBar = document.createElement("div");
    this.settingsBar.classList.add("settings-bar");
    const settingsBarClose = document.createElement("div");
    settingsBarClose.classList.add("settings-bar__close", "close");
    this.settingsBar.appendChild(settingsBarClose);
    settingsBarClose.addEventListener('click', () => this.toggleSettingsBar())
    this.navListItems.forEach((item, index) => {
      const settingsBarItem = document.createElement("div");
      settingsBarItem.classList.add("settings-bar__item");
      settingsBarItem.dataset.itemId = index;
      const settingsBarItemTitle = document.createElement("h3");
      settingsBarItemTitle.classList.add('title', "title_md");
      settingsBarItemTitle.textContent = item.label;
      const settingsBarOptions = document.createElement("div");
      settingsBarOptions.classList.add('options');
      const options = this.createOptionsMarkup({type: item.label, options: item.options});
      settingsBarOptions.innerHTML = options;
      settingsBarItem.appendChild(settingsBarItemTitle);
      settingsBarItem.appendChild(settingsBarOptions);
      this.settingsBar.appendChild(settingsBarItem);
    })
    document.body.appendChild(this.settingsBar);
    this.addSettingsEventListener();
  }

  createOptionsMarkup({type, options}) {
    return options.map((item, index) => {
      return `<div class="option">
      <div class="option__item"><div ${type !== 'Material' ? `style="background-color:` + item.parameters.color + '"' : ''} class="option__box${type !== 'Material' ? ' option__box_color' : ''}" data-option-type="${type}" data-option-value="${item.name}">${type === 'Material' ? item.name : ''}</div>
     </div>
      </div>`
    }).join('');
  }

  handleNavItemClick(el){
    if (!el?.dataset?.itemId) return
    this.toggleSettingsBar(el?.dataset?.itemId)
  }

  toggleSettingsBar(itemId) {
    const setClosed = this.activeMenuItem === itemId || (!itemId && itemId !== 0);

    setClosed 
    ? this.settingsBar.classList.remove('active') 
    : this.settingsBar.classList.add('active');
    const sideBarItems = this.settingsBar.querySelectorAll(`.settings-bar__item`);
    sideBarItems.forEach(item => {
      item.style.display = (setClosed || item.dataset.itemId !== itemId) ? 'none' : 'block';
      this.activeMenuItem = setClosed ? null : itemId;
    })
  }

  addSettingsEventListener() {
    let settingsItems = this.settingsBar.querySelectorAll('.option__box');
    settingsItems.forEach(item => {
      item.addEventListener("click", ev => {
        let type = item.dataset.optionType;
        let value = item.dataset.optionValue;
        this.handleSettingsClick({type, value})
      })
    })
  }
  
  handleSettingsClick({type, value}) {
    switch (type) {
      case "Material": {
        if (this.viewer.objectState.material === value) return;
        this.viewer.modelObject.scene.traverse(obj => {
          if (obj.name === "Mesh") {
            const data = materials.find(obj => obj.name === value)
            const mapSrc = data.parameters.map;
            const normalMapSrc = data.parameters.normalMap;
            const occlusionMapSrc = data.parameters.occlusionMap;
            obj.material.map = mapSrc;
            obj.material.normalMap = normalMapSrc;
            obj.material.roughnessMap = occlusionMapSrc;
            obj.material.metalnessMap = occlusionMapSrc;
            this.viewer.objectState.material = value;
          }
        })
        break;
      }
      case "Color": {
        if (this.viewer.objectState.color === value) return;
        this.viewer.modelObject.scene.traverse(obj => {
          if (obj.name === "Mesh" || obj.name === "Mesh_2") {
            this.viewer.objectState.color = value;
          }

          if (obj.name === "Mesh") {
            const data = colors.find(obj => obj.name === value);
            let bagColor = new Color(data.parameters.bagColor).getHex();
            obj.material.color.set(bagColor);
          }

          if (obj.name === "Mesh_2") {
            const data = colors.find(obj => obj.name === value);
            let strapColor = new Color(data.parameters.strapColor).getHex();
            obj.material.color.set(strapColor);
          }
          
        })
        break;
      }
      case "Furniture": {
        if (this.viewer.objectState.furniture === value) return;
        this.viewer.modelObject.scene.traverse(obj => {
          if (obj.name === "Mesh_1") {
            const data = furnitures.find(obj => obj.name === value);
            let furnitureColor = new Color(data?.parameters?.color).getHex();
            obj.material.color.set(furnitureColor);
            this.viewer.objectState.furniture = value;
          }
        })
        break;
      }
    }
    
  }
}
