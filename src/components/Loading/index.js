import loadingIcon from './../../assets/loading.gif';

export default class Loading {
  constructor(container) {
    this.container = container;
    this.init();
  }

  init() {
    this.wrapp = document.querySelector('.loader');
    let img = document.createElement('img');
    img.src = loadingIcon;
    img.className = 'loader__img';
    this.wrapp
    this.wrapp.appendChild(img);
    // this.show();
  }

  show() {
    this.wrapp.classList.add('shown');
    // this.container.style.pointerEvents = 'none';
  }

  hide() {
    this.closetimeout && clearTimeout(this.closetimeout);
    this.closetimeout = setTimeout(() => {
      this.wrapp.classList.remove('shown');
      // this.container.style.pointerEvents = 'auto';
    }, 200);
  }
}
