import { TextureLoader } from "three"
import denim from "./../assets/backpack/denim_baseColor.jpg"
import denimNormal from "./../assets/backpack/denim_baseColor.jpg"
import denimOcclusion from "./../assets/backpack/denim_baseColor.jpg"
import fabric from "./../assets/backpack/fabric_baseColor.jpg"
import fabricNormal from "./../assets/backpack/fabric_baseColor.jpg"
import fabricOcclusion from "./../assets/backpack/fabric_baseColor.jpg"
import leather from "./../assets/backpack/leather_baseColor.jpg"
import leatherNormal from "./../assets/backpack/leather_baseColor.jpg"
import leatherOcclusion from "./../assets/backpack/leather_baseColor.jpg"

const denimMap = new TextureLoader().load(denim);
const denimNormalMap = new TextureLoader().load(denimNormal);
const denimOcclusionMap = new TextureLoader().load(denimOcclusion);

const fabricMap = new TextureLoader().load(fabric);
const fabricNormalMap = new TextureLoader().load(fabricNormal);
const fabricOcclusionMap = new TextureLoader().load(fabricOcclusion);

const leatherMap = new TextureLoader().load(leather);
const leatherNormalMap = new TextureLoader().load(leatherNormal);
const leatherOcclusionMap = new TextureLoader().load(leatherOcclusion);

export const materials = [
    {
        name: "Denim",
        parameters: {
            img: denim,
            map: denimMap,
            normalMap: denimNormalMap,
            occlusionMap: denimOcclusionMap,
        }
    },
    {
        name: "Fabric",
        parameters: {
            img: fabric,
            map: fabricMap,
            normalMap: fabricNormalMap,
            occlusionMap: fabricOcclusionMap,
        }
    },
    {
        name: "Leather",
        parameters: {
            img: leather,
            map: leatherMap,
            normalMap: leatherNormalMap,
            occlusionMap: leatherOcclusionMap,
        }
    },
]

export const colors = [
    {
        name: "Brown",
        parameters: {
            color: "#9f7356",
            bagColor: "#9f7356",
            strapColor: "#9f7356",
        }
    },
    {
        name: "Black",
        parameters: {
            color: "#383838",
            bagColor: "#383838",
            strapColor: "#383838",
        }
    },
    {
        name: "Blue",
        parameters: {
            color: '#112959',
            bagColor: '#112959',
            strapColor: '#112959',
        }
    },
]

export const furnitures = [
    {
        name: "Silver",
        parameters: {
            color: "#fefefe",
        }
    },
    {
        name: "Black",
        parameters: {
            color: "#000000",
        }
    },
    {
        name: "Gold",
        parameters: {
            color: '#e7cb18',
        }
    },
]