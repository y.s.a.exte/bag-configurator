import * as THREE from 'three';
import App from './components/app';
import './scss/style.scss';

// Import Styles

window.THREE = THREE;
new App();
